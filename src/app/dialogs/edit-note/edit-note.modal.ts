import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Note } from '@components/notes/notes.interface';

@Component({
  selector: "edit-note",
  templateUrl: "./edit-note.modal.html",
  styleUrls: ["./edit-note.modal.scss"],
})
export class EditNote {
  noteForm: FormGroup;
  editMode: Boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Note,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.editMode = !this.data;
    this.setForm();
  }

  dataHasChanged() {
    return this.noteForm.get('authorName').value !== this.data?.authorName ||
    this.noteForm.get('content').value !== this.data?.content ||
    this.noteForm.get('date').value !== this.data?.date;
  }

  private setForm() {
    this.noteForm = this.formBuilder.group({
      authorName: [ this.data?.authorName || null, { validators: [ Validators.required ] } ],
      content: [ this.data?.content || null, { validators: [ Validators.required ] } ],
      date: [ this.data?.date || Date.now() ]
    });
  }
}
