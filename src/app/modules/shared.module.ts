import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

// components
import { CardComponent } from '@components/card/card.component';

// directives

@NgModule({
  declarations: [
    CardComponent,
  ],
  exports: [
    FormsModule,
    RouterModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,
    CardComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
