import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { NgModule } from "@angular/core";
import { SharedModule } from "@modules/shared.module";
import { EditNote } from "@dialogs/edit-note/edit-note.modal";

@NgModule({
  imports: [SharedModule],
  declarations: [EditNote],
  entryComponents: [EditNote],
  exports: [EditNote],
  providers: [
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        disableClose: true,
        hasBackdrop: true
      }
    }
  ]
})
export class DialogsModule {}
