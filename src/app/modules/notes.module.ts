import { NgModule } from '@angular/core';
import { SharedModule } from 'app/modules/shared.module';
import { NotesListComponent } from '@components/notes/notes-list/notes-list.component';
import { NewNoteComponent } from '@components/notes/new-note/new-note.component';
import { NoteComponent } from '@components/notes/note/note.component';

@NgModule({
  declarations: [
    NoteComponent,
    NewNoteComponent,
    NotesListComponent
  ],
  exports: [
    NoteComponent,
    NewNoteComponent,
    NotesListComponent
  ],
  imports: [SharedModule]
})
export class NotesModule { }
