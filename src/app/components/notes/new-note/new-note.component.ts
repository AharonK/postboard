import { Component } from "@angular/core";
import { NotesService } from "../notes.service";
import { DialogService } from "@services/dailog/dialog.service";

@Component({
  selector: "new-note",
  templateUrl: "./new-note.component.html",
  styleUrls: ["./new-note.component.scss"],
})
export class NewNoteComponent {
  constructor(
    private dialogService: DialogService,
    private notesService: NotesService
  ) {}

  addNew() {
    this.dialogService
      .editNote()
      .afterClosed()
      .subscribe((data) => {
        data && this.notesService.addNote(data);
      });
  }
}
