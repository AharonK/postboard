import { Component, Input } from "@angular/core";
import { Note } from "../notes.interface";
import { DialogService } from "@services/dailog/dialog.service";
import { NotesService } from "../notes.service";

@Component({
  selector: "note",
  templateUrl: "./note.component.html",
  styleUrls: ["./note.component.scss"],
})
export class NoteComponent {
  @Input() data: Note;
  @Input() idx: number;

  constructor(
    private dialogService: DialogService,
    private notesService: NotesService
  ) {}

  openNote() {
    this.dialogService
      .editNote(this.data)
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          if (data.delete) {
            this.notesService.deleteNote(this.idx);
          } else {
            this.notesService.editNote(this.idx, data);
          }
        }
      });
  }
}
