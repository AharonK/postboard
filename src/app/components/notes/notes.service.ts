import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { DataService } from "@services/data/data.service";
import { Note } from "./notes.interface";
import cloneDeep from "lodash/cloneDeep";
import { NotesModule } from '@modules/notes.module';

const DATA_KEY = "POST_BOARD_NOTES";

@Injectable({ providedIn: NotesModule })
export class NotesService {
  private _notesList$ = new BehaviorSubject<Note[]>([]);
  notesList$ = this._notesList$.asObservable();
  get notesList(): Note[] {
    return this._notesList$.getValue();
  }

  constructor(private dataService: DataService) {
    this.init();
  }

  init() {
    let notes = this.dataService.getData(DATA_KEY) || [];
    this._notesList$.next(notes);
  }

  addNote(note: Note) {
    let notes: Note[] = cloneDeep(this.notesList);
    notes.push(note);
    this.setNotes(notes);
  }

  editNote(noteIdx: number, updatedNote: Note) {
    let notes: Note[] = cloneDeep(this.notesList);
    notes[noteIdx] = updatedNote;
    this.setNotes(notes);
  }

  deleteNote(noteIdx: number) {
    let notes: Note[] = cloneDeep(this.notesList);
    notes.splice(noteIdx, 1);
    this.setNotes(notes);
  }

  private setNotes(notes: Note[]) {
    this.dataService.setData(DATA_KEY, notes);
    this._notesList$.next(notes);
  }
}
