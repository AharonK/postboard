import { Component } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { NotesService } from "../notes.service";
import { Note } from "../notes.interface";

@Component({
  selector: "notes-list",
  templateUrl: "./notes-list.component.html"
})
export class NotesListComponent {
  notes$: Observable<Note[]> = this.notesService.notesList$;

  constructor(private notesService: NotesService) {}
}
