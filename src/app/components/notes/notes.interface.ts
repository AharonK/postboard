export interface Note {
  authorName: string;
  content: string;
  date: number;
}