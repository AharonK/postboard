import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "@services/auth/auth.service";

@Component({
  selector: "login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.setForm();
  }

  login() {
    this.authService.login();
  }

  private setForm() {
    this.loginForm = this.formBuilder.group({
      username: [null, { validators: [Validators.required] }],
    });
  }
}
