import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "@services/auth/auth.service";

@Injectable({ providedIn: 'root' })
export class LoginResolver implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {}

  canActivate(): Observable<boolean> {
    return new Observable((observer) => {
      this.authService.isLoggedIn$.subscribe((loggedIn) => {
        if (loggedIn) {
          this.router.navigate(["./notes"]);
          observer.next(false);
        } else {
          observer.next(true);
        }
      });
    });
  }
}
