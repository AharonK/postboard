import { CanActivate, Router } from "@angular/router";
import { Injectable } from "@angular/core";

import { AuthService } from "@services/auth/auth.service";

@Injectable()
export class NotesResolver implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService) {}

  canActivate(): boolean {
    if (this.authService.isLoggedIn) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
