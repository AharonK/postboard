import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NotesPage } from './notes.page';
import { NotesResolver } from './notes.resolver';

const notesRoutes: Routes = [
  {
    // /notes
    path: "",
    component: NotesPage,
    canActivate: [NotesResolver]
  },
];

@NgModule({
  imports: [RouterModule.forChild(notesRoutes)],
  exports: [RouterModule],
})
export class NotesRoutingModule {}
