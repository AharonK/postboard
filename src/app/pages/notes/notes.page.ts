import { Component } from "@angular/core";

@Component({
  selector: "notes",
  template: `
    <div class="notesPage maxHeight">
      <notes-list></notes-list>
    </div>
  `,
  styles: [
    `.notesPage {
      padding: 0 20px;
    }`
  ]
})
export class NotesPage { }
