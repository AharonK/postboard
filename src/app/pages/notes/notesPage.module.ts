import { NgModule } from '@angular/core';
import { NotesModule } from '@modules/notes.module';
import { NotesPage } from './notes.page';
import { NotesResolver } from './notes.resolver';
import { NotesRoutingModule } from './notes-routing.module';

@NgModule({
  declarations: [
    NotesPage
  ],
  imports: [
    NotesModule,
    NotesRoutingModule
  ],
  providers: [NotesResolver]
})
export class NotesPageModule { }
