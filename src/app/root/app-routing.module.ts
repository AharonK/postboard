import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from '@pages/login/login.page';
import { LoginResolver } from '@pages/login/login.resolver';

const routes: Routes = [
  {
    // /login
    path: 'login',
    component: LoginPage,
    canActivate: [LoginResolver]
  },
  {
    // /notes
    path: 'notes',
    loadChildren: () => import('app/pages/notes/notesPage.module').then(mod => mod.NotesPageModule)
  },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
