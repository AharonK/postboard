import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'app/root/app-routing.module';
import { DialogsModule } from '@modules/dailog.module';
import { SharedModule } from '@modules/shared.module';
import { AppComponent } from 'app/root/app.component';
import { LoginPage } from '@pages/login/login.page';
import { LoginResolver } from '@pages/login/login.resolver';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
  ],
  imports: [
    SharedModule,         // contains all shared components/pipes/directives
    DialogsModule,        // contains all modals
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [LoginResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
