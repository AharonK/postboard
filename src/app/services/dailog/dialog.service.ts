import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EditNote } from '@dialogs/edit-note/edit-note.modal';
import { Note } from '@components/notes/notes.interface';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(public dialog: MatDialog) { }

  editNote(note?: Note): MatDialogRef<EditNote> {
    return this.dialog.open(EditNote, {
      width: '500px',
      data: note || null
    });
  }
}