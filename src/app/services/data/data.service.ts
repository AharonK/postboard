import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DataService {
  getData(dataKey) {
    return JSON.parse(localStorage.getItem(dataKey))
  }

  setData(dataKey, data)  {
   localStorage.setItem(dataKey, JSON.stringify(data));
  }
}