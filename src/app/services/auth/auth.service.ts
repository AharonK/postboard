import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

const LOGGED_IN_KEY = 'IS_LOGGED_IN';

@Injectable({ providedIn: 'root' })
export class AuthService {
  readonly isLoggedIn$ = new BehaviorSubject<boolean>(false);
  get isLoggedIn() { return this.isLoggedIn$.getValue(); }
  
  constructor() { this.init() }

  login() {
    localStorage.setItem(LOGGED_IN_KEY, 'true');
    this.isLoggedIn$.next(true);
  }

  private init() {
    const isLoggedIn = JSON.parse(localStorage.getItem(LOGGED_IN_KEY))
    isLoggedIn && this.isLoggedIn$.next(true);
  }
}
